use std::{
    collections::HashSet,
    sync::atomic::{AtomicI32, Ordering::SeqCst},
};

use crate::{letters::Sorted, words_group::Words};

use rayon::prelude::*;

enum ProgressTestResult {
    Hopeless,
    StillProgressing,
    Finishing,
}

pub struct Gluer {
    searched_letters: Sorted,
}

impl Gluer {
    pub fn new_gluer(letters: Sorted) -> Self {
        Gluer {
            searched_letters: letters,
        }
    }

    pub fn check_word<'a>(&'a self, words: &'a Vec<&'a str>) -> HashSet<Words> {
        let total_words_count = words.len();
        let total_words_chars_len = total_words_count.to_string().len();
        println!("Total iterations expected: [{}]", total_words_count);

        let counter = AtomicI32::new(0);

        let found_matched: HashSet<_> = words
            .into_par_iter()
            .map(|next_word| {
                let next_word_position = words.iter().position(|elem| elem == next_word).unwrap();
                let remaining_words = &words[(next_word_position + 1)..];

                let remaining_letters = self.searched_letters.without_str(next_word);

                let iteration_matched = self.search_for_expansion(
                    &remaining_words,
                    Words::from(next_word),
                    &remaining_letters,
                );

                counter.fetch_add(1, SeqCst);
                println!(
                    "Finished {:width$}/{}",
                    counter.load(SeqCst),
                    total_words_count,
                    width = total_words_chars_len
                );

                iteration_matched
            })
            .flatten()
            .collect();

        found_matched
    }

    pub fn search_for_expansion<'a>(
        &'a self,
        words: &'a [&'a str],
        base_word: Words<'a>,
        remaining_letters: &Sorted,
    ) -> HashSet<Words> {
        let mut found: HashSet<Words> = HashSet::new();

        for next_word in words {
            let mut collected_letters: Words = base_word.clone();

            let progress_test_result =
                self.test_collection_progress(&collected_letters, remaining_letters, next_word);
            match progress_test_result {
                ProgressTestResult::Hopeless => {
                    collected_letters.clear();
                    continue;
                }
                ProgressTestResult::Finishing => {
                    continue;
                }
                ProgressTestResult::StillProgressing => {}
            }

            collected_letters.push_str(next_word);

            let is_anagram = self
                .searched_letters
                .eq_anagram_str(&collected_letters.merged());
            if is_anagram {
                found.insert(collected_letters);

                return found;
            }

            let future_words = slice_after(&next_word, words);
            let remaining_letters = remaining_letters.without_str(next_word);

            let searching_result = self.search_for_expansion(
                future_words,
                collected_letters.clone(),
                &remaining_letters,
            );

            found.extend(searching_result);
        }

        found
    }

    fn test_collection_progress(
        &self,
        collected: &Words,
        remaining_letters: &Sorted,
        next_word: &str,
    ) -> ProgressTestResult {
        if collected.group_size() > 3 {
            return ProgressTestResult::Hopeless;
        }

        if collected.len() > self.searched_letters.len() {
            return ProgressTestResult::Hopeless;
        }

        let missing_letters = !remaining_letters.contains_str(next_word);
        if missing_letters {
            return ProgressTestResult::Finishing;
        }

        ProgressTestResult::StillProgressing
    }
}

fn slice_after<'a>(boundary_word: &'a str, all_words: &'a [&'a str]) -> &'a [&'a str] {
    let new_head_position = all_words
        .iter()
        .position(|inner_word| inner_word == &boundary_word)
        .unwrap();

    &all_words[new_head_position..]
}

#[cfg(test)]
mod test {
    use crate::Sorted;

    use super::*;

    #[test]
    fn test_glue_then_match() {
        let words: &Vec<&str> = &vec!["abc", "def", "ghi", "cde"];

        let searched_letters = Sorted::from_str("abcdefghi");
        let gluer = Gluer::new_gluer(searched_letters);
        let result = gluer.check_word(words);
        println!("{:?}", result);

        let mut expected_result = Words::from("abc");
        expected_result.push_str("def");
        expected_result.push_str("ghi");
        assert!(result.contains(&expected_result));
    }
}
