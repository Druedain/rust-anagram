use std::{collections::BTreeMap, fs};

use crate::{letters::Sorted, strings::capitalize};

pub fn filter_words<'a>(polish_words_path: &'a str, name_letters: &'a Sorted) -> Vec<String> {
    let file: String = fs::read_to_string(polish_words_path).unwrap();
    let sorted_letters: BTreeMap<String, Sorted> = file
        .split_whitespace()
        .into_iter()
        .map(|word| {
            let capitalized: String = capitalize(word);
            (capitalized, Sorted::from_str(word))
        })
        .collect::<_>();

    let filtered_letters: BTreeMap<String, Sorted> = sorted_letters
        .into_iter()
        .filter(|(_, v)| name_letters.contains(&v))
        .collect();

    filtered_letters.into_keys().collect()
}
