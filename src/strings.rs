pub fn capitalize(input_str: &str) -> String {
    let lowercase = input_str.to_lowercase();
    let mut chars = lowercase.chars();

    if let Some(char) = chars.next() {
        char.to_uppercase().collect::<String>() + chars.as_str()
    } else {
        String::new()
    }
}
