use std::fmt::Display;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Sorted {
    letters: Vec<char>,
}

impl Display for Sorted {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let merged: String = self.letters.iter().collect();
        write!(f, "{}", merged)
    }
}

impl Sorted {
    pub fn from_str(source: &str) -> Self {
        let mut chars: Vec<_> = to_lowercase_chars(source);
        chars.sort();

        Sorted { letters: chars }
    }

    pub fn len(&self) -> usize {
        self.letters.len()
    }

    pub fn eq_anagram_str(&self, that: &str) -> bool {
        let this = self.to_string();
        let that = sorted_str(that);

        this.eq_ignore_ascii_case(&that)
    }

    pub fn contains(&self, that: &Sorted) -> bool {
        let that: String = that.letters.iter().collect();

        self.contains_str(&that)
    }

    pub fn contains_str(&self, that: &str) -> bool {
        let this = self.without_str(that);

        self.letters.len() == this.len() + that.len()
    }

    pub fn without_str(&self, that: &str) -> Sorted {
        let mut this = self.letters.clone();

        that.chars()
            .for_each(|that_char| remove_char(&mut this, &that_char));

        let new_content: String = this.iter().collect();

        Sorted::from_str(&new_content)
    }
}

pub fn sorted_str(to_sort: &str) -> String {
    Sorted::from_str(to_sort).to_string()
}

fn to_lowercase_chars(word: &str) -> Vec<char> {
    word.to_lowercase().chars().collect()
}

fn remove_char(chars: &mut Vec<char>, to_remove: &char) {
    if let Some(position) = chars.iter().position(|char| char == to_remove) {
        chars.remove(position);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn letter_from_unsorted_has_same_letters_lower_as_str_sorted() {
        let has_same_words = Sorted::from_str("za").eq_anagram_str("az");

        assert!(has_same_words);
    }

    #[test]
    fn letter_from_sorted_has_same_letters_lower_as_str_unsorted() {
        let has_same_words = Sorted::from_str("az").eq_anagram_str("za");

        assert!(has_same_words);
    }

    #[test]
    fn letters_without_string() {
        let subtracted = Sorted::from_str("acdb").without_str("ab");

        assert!(subtracted.eq_anagram_str("cd"));
        assert_eq!(subtracted.len(), 2);
    }
}
