#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Words<'a> {
    group: Vec<&'a str>,
    merged: String,
}

impl<'a> Words<'a> {
    pub fn new() -> Self {
        Words {
            group: Vec::new(),
            merged: String::new(),
        }
    }

    pub fn from(first_word: &'a str) -> Self {
        let mut new = Words::new();
        new.push_str(first_word);

        new
    }

    pub fn push_str(&mut self, next_word: &'a str) {
        self.group.push(next_word);
        self.merged.push_str(next_word);
    }

    pub fn len(&self) -> usize {
        self.merged.len()
    }

    pub fn clear(&mut self) {
        self.group.clear();
        self.merged.clear();
    }

    pub fn merged(&self) -> &str {
        &self.merged
    }

    pub fn group(self) -> Vec<&'a str> {
        self.group
    }

    pub fn group_size(&self) -> usize {
        self.group.len()
    }
}
